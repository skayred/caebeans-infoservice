package ru.susu.caebeans;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import ru.susu.caebeans.database.DatabaseManager;
import ru.susu.caebeans.database.DatabaseManagerImpl;
import ru.susu.caebeans.lib.Config;
import ru.susu.caebeans.lib.ConfigImpl;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/27/12
 * Time: 9:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class InfoServiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(DatabaseManager.class).to(DatabaseManagerImpl.class).in(Singleton.class);
        bind(Config.class).to(ConfigImpl.class).in(Singleton.class);

//        bind(TaskBuilder.class).to(TaskBuilderImpl.class);
//        bind(Executor.class).to(ExecutorImpl.class);
    }
}
