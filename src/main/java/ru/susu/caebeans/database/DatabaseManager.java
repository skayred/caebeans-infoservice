package ru.susu.caebeans.database;

import ru.susu.caebeans.models.CAEPackage;
import ru.susu.caebeans.models.License;
import ru.susu.caebeans.models.LicenseType;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/27/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DatabaseManager {
    List<LicenseType> getLicensesByPackage(int packageID);

    List<CAEPackage> getPackages();
}
