package ru.susu.caebeans.database;

import com.google.inject.Inject;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.susu.caebeans.models.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/27/12
 * Time: 9:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseManagerImpl implements DatabaseManager {
    private final SessionFactory sessionFactory;

    @Inject
    DatabaseManagerImpl() {
        sessionFactory = CAESessionFactory.getSessionFactory();
    }

    @Override
    public List<LicenseType> getLicensesByPackage(int packageID) {
        List<LicenseType> result = new ArrayList<LicenseType>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List packages = session.createSQLQuery("SELECT lt.* FROM LicenseType lt, LicenseTypeForPackage ltp WHERE ltp.lt_id=lt.lt_id AND ltp.package_id=" + packageID + "").addEntity(LicenseType.class).list();
            for (Iterator iterator = packages.iterator(); iterator.hasNext(); ) {
                LicenseType caePackage = (LicenseType) iterator.next();

                result.add(caePackage);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    @Override
    public List<CAEPackage> getPackages() {
        List<CAEPackage> result = new ArrayList<CAEPackage>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List packages = session.createSQLQuery("SELECT * FROM CAEPackage").addEntity(CAEPackage.class).list();
            for (Iterator iterator = packages.iterator(); iterator.hasNext(); ) {
                CAEPackage caePackage = (CAEPackage) iterator.next();

                result.add(caePackage);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }
}
