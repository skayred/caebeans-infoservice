package ru.susu.caebeans.wsrf;

import ru.susu.caebeans.models.CAEPackage;
import ru.susu.caebeans.models.License;
import ru.susu.caebeans.models.LicenseType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/27/12
 * Time: 9:17 PM
 * To change this template use File | Settings | File Templates.
 */
@WebService(targetNamespace = "http://caebeans.susu.ru", portName = "InfoService")
public interface InfoService  {

    @WebMethod(action = "http://caebeans.susu.ru/getLicenses")
    List<LicenseType> getLicenses(int packageID);

    @WebMethod(action = "http://caebeans.susu.ru/getPackages")
    List<CAEPackage> getPackages();
}