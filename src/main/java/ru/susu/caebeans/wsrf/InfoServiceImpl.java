package ru.susu.caebeans.wsrf;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import ru.susu.caebeans.InfoServiceModule;
import ru.susu.caebeans.database.DatabaseManager;
import ru.susu.caebeans.models.*;

import javax.jws.WebService;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/27/12
 * Time: 9:33 PM
 * To change this template use File | Settings | File Templates.
 */
@WebService(endpointInterface = "ru.susu.caebeans.wsrf.InfoService")
public class InfoServiceImpl implements InfoService {

    @Inject
    private DatabaseManager databaseManager;

    public InfoServiceImpl() {
        Injector injector = Guice.createInjector(new InfoServiceModule());
        injector.injectMembers(this);
    }

    @Override
    public List<LicenseType> getLicenses(int packageID) {
        return databaseManager.getLicensesByPackage(packageID);
    }

    @Override
    public List<CAEPackage> getPackages() {
        return databaseManager.getPackages();
    }
}
