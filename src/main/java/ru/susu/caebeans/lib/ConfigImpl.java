package ru.susu.caebeans.lib;

import com.google.inject.Inject;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/28/12
 * Time: 9:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigImpl implements Config {
    public static final String CAEBEANS_CAESERVER_PROPERTIES = ".caebeans/infoservice.properties";
    private PropertiesConfiguration config;

    @Inject
    ConfigImpl() {
        try {
            config = new PropertiesConfiguration(CAEBEANS_CAESERVER_PROPERTIES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Configuration getConfig() {
        return config;
    }
}
