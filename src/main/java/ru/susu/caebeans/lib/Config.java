package ru.susu.caebeans.lib;

import org.apache.commons.configuration.Configuration;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/28/12
 * Time: 9:03 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Config {
    public Configuration getConfig();
}
