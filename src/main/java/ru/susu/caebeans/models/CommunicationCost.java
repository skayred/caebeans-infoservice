package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/2/12
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "CommunicationCost")
public class CommunicationCost {
    @Id
    @Column(name = "cc_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "fromTSFID")
    private int fromTSFID;
    @Column(name = "toTSFID")
    private int toTSFID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromTSFID() {
        return fromTSFID;
    }

    public void setFromTSFID(int fromTSFID) {
        this.fromTSFID = fromTSFID;
    }

    public int getToTSFID() {
        return toTSFID;
    }

    public void setToTSFID(int toTSFID) {
        this.toTSFID = toTSFID;
    }
}