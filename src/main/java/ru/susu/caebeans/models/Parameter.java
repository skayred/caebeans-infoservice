package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/3/12
 * Time: 9:54 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Parameter")
public class Parameter {
    @Id
    @Column(name = "parameter_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "category_id")
    private int categoryID;
    @Column(name = "node_grid_count")
    private int nodeGridCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getNodeGridCount() {
        return nodeGridCount;
    }

    public void setNodeGridCount(int nodeGridCount) {
        this.nodeGridCount = nodeGridCount;
    }
}