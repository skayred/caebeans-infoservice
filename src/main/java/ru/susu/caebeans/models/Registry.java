package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/3/12
 * Time: 9:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Registry")
public class Registry {
    @Id
    @Column(name = "registry_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "uri")
    private String uri;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
