package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/3/12
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Resource")
public class Resource {
    @Id
    @Column(name = "resource_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "cpu_arch_name")
    private String cpuArchName;
    @Column(name = "os_id")
    private int osID;
    @Column(name = "core_count")
    private int coreCount;
    @Column(name = "cpu_count")
    private int cpuCount;
    @Column(name = "node_count")
    private int nodeCount;
    @Column(name = "memory_per_node")
    private int memoryPerNode;
    @Column(name = "size_input_files")
    private int sizeInputFiles;
    @Column(name = "size_output_files")
    private int sizeOutputFiles;
    @Column(name = "estimated_time")
    private int estimatedTime;
    @Column(name = "real_time")
    private int realTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCpuArchName() {
        return cpuArchName;
    }

    public void setCpuArchName(String cpuArchName) {
        this.cpuArchName = cpuArchName;
    }

    public int getOsID() {
        return osID;
    }

    public void setOsID(int osID) {
        this.osID = osID;
    }

    public int getCoreCount() {
        return coreCount;
    }

    public void setCoreCount(int coreCount) {
        this.coreCount = coreCount;
    }

    public int getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    public int getMemoryPerNode() {
        return memoryPerNode;
    }

    public void setMemoryPerNode(int memoryPerNode) {
        this.memoryPerNode = memoryPerNode;
    }

    public int getSizeInputFiles() {
        return sizeInputFiles;
    }

    public void setSizeInputFiles(int sizeInputFiles) {
        this.sizeInputFiles = sizeInputFiles;
    }

    public int getSizeOutputFiles() {
        return sizeOutputFiles;
    }

    public void setSizeOutputFiles(int sizeOutputFiles) {
        this.sizeOutputFiles = sizeOutputFiles;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getRealTime() {
        return realTime;
    }

    public void setRealTime(int realTime) {
        this.realTime = realTime;
    }
}
