package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/4/12
 * Time: 8:49 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TargetSystemFactory")
public class TargetSystemFactory {
    @Id
    @Column(name = "tsf_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "registry_id")
    private int registryID;
    @Column(name = "name")
    private String name;
    @Column(name = "uri")
    private String uri;
    @Column(name = "cpu_count")
    private int cpuCount;
    @Column(name = "node_count")
    private int nodeCount;
    @Column(name = "memory_per_node")
    private int mempryPerNode;
    @Column(name = "cpu_arch_name")
    private String cpuArchName;
    @Column(name = "os_id")
    private int osID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRegistryID() {
        return registryID;
    }

    public void setRegistryID(int registryID) {
        this.registryID = registryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    public int getMempryPerNode() {
        return mempryPerNode;
    }

    public void setMempryPerNode(int mempryPerNode) {
        this.mempryPerNode = mempryPerNode;
    }

    public String getCpuArchName() {
        return cpuArchName;
    }

    public void setCpuArchName(String cpuArchName) {
        this.cpuArchName = cpuArchName;
    }

    public int getOsID() {
        return osID;
    }

    public void setOsID(int osID) {
        this.osID = osID;
    }
}
