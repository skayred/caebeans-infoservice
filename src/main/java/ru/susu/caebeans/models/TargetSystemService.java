package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/4/12
 * Time: 8:53 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TargetSystemService")
public class TargetSystemService {
    @Id
    @Column(name = "tss_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "tsf_id")
    private int tsfID;
    @Column(name = "uri")
    private String uri;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTsfID() {
        return tsfID;
    }

    public void setTsfID(int tsfID) {
        this.tsfID = tsfID;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
