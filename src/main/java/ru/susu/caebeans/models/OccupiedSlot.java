package ru.susu.caebeans.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/3/12
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "OccupiedSlot")
public class OccupiedSlot {
    @Id
    @Column(name = "slot_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "tsf_id")
    private int tsfID;
    @Column(name = "start_time")
    private Date startTime;
    @Column(name = "stop_time")
    private Date stopTime;
    @Column(name = "task_id")
    private int taskID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTsfID() {
        return tsfID;
    }

    public void setTsfID(int tsfID) {
        this.tsfID = tsfID;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public int getTaskID() {
        return taskID;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }
}
