package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/2/12
 * Time: 9:34 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Application")
public class Application {
    @Id
    @Column(name = "application_id")
    private int applicationID;
    @Column(name = "lt_id")
    private int ltID;
    @Column(name = "package_id")
    private int packageID;

    public int getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(int applicationID) {
        this.applicationID = applicationID;
    }

    public int getLtID() {
        return ltID;
    }

    public void setLtID(int ltID) {
        this.ltID = ltID;
    }

    public int getPackageID() {
        return packageID;
    }

    public void setPackageID(int packageID) {
        this.packageID = packageID;
    }
}
