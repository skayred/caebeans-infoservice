package ru.susu.caebeans.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/3/12
 * Time: 9:42 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "LicenseType")
public class LicenseType {
    @Id
    @Column(name = "lt_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "version")
    private String version;
    @Column(name = "core_count")
    private int coreCount;
    @Column(name = "core_per_calculation")
    private int corePerCalculation;
    @Column(name = "cpu_count")
    private int cpuCount;
    @Column(name = "node_count")
    private int nodeCount;
    @Column(name = "simultaneous_calc_num")
    private int simultaneousCalcNumber;
    @Column(name = "nodeGridCount")
    private int nodeGridCount;
    @Column(name = "workspaceNumber")
    private int workspaceNumber;
    @Column(name = "expiration_date")
    private Date expirationDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getCoreCount() {
        return coreCount;
    }

    public void setCoreCount(int coreCount) {
        this.coreCount = coreCount;
    }

    public int getCorePerCalculation() {
        return corePerCalculation;
    }

    public void setCorePerCalculation(int corePerCalculation) {
        this.corePerCalculation = corePerCalculation;
    }

    public int getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    public int getSimultaneousCalcNumber() {
        return simultaneousCalcNumber;
    }

    public void setSimultaneousCalcNumber(int simultaneousCalcNumber) {
        this.simultaneousCalcNumber = simultaneousCalcNumber;
    }

    public int getNodeGridCount() {
        return nodeGridCount;
    }

    public void setNodeGridCount(int nodeGridCount) {
        this.nodeGridCount = nodeGridCount;
    }

    public int getWorkspaceNumber() {
        return workspaceNumber;
    }

    public void setWorkspaceNumber(int workspaceNumber) {
        this.workspaceNumber = workspaceNumber;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}

