package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/3/12
 * Time: 9:49 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "LicenseTypeForPackage")
public class LicenseTypeForPackage {
    @Id
    @Column(name = "ltp_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "lt_id")
    private int ltID;
    @Column(name = "package_id")
    private int packageID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLtID() {
        return ltID;
    }

    public void setLtID(int ltID) {
        this.ltID = ltID;
    }

    public int getPackageID() {
        return packageID;
    }

    public void setPackageID(int packageID) {
        this.packageID = packageID;
    }
}
