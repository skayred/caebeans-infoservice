package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/27/12
 * Time: 9:29 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "CAEPackage")
public class CAEPackage {
    @Id
    @Column(name = "package_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int packageID;
    @Column(name = "name")
    private String name;
    @Column(name = "version")
    private String version;

    public int getId() {
        return packageID;
    }

    public void setId(int id) {
        this.packageID = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
