package ru.susu.caebeans.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/4/12
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "ServiceOnTSF")
public class ServiceOnTSF {
    @Id
    @Column(name = "service_on_tsf_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "service_id")
    private int serviceID;
    @Column(name = "tsf_id")
    private int tsfID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public int getTsfID() {
        return tsfID;
    }

    public void setTsfID(int tsfID) {
        this.tsfID = tsfID;
    }
}
