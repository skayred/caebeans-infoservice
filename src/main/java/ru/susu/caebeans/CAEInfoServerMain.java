package ru.susu.caebeans;

import com.google.inject.Guice;
import com.google.inject.Injector;
import ru.susu.caebeans.lib.Config;
import ru.susu.caebeans.wsrf.InfoServiceImpl;

import javax.xml.ws.Endpoint;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/27/12
 * Time: 9:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class CAEInfoServerMain {
    public static final String SERVER_HOST = "infoservice.host";
    public static final String SERVER_PORT = "infoservice.port";
    public static final String SERVER_PATH = "infoservice";

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new InfoServiceModule());
        Config config = injector.getInstance(Config.class);

        String serverHost = getServerHost(config);

        Endpoint.publish(serverHost + SERVER_PATH, new InfoServiceImpl());
    }

    private static String getServerHost(Config config) {
        String configHost = config.getConfig().getString(SERVER_HOST);
        String configPort = config.getConfig().getString(SERVER_PORT);

        String host = configHost + ":" + configPort + "/";

        return host;
    }
}
